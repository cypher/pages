#+title: Bold Men of God
#+date: <Wed Sep  7 05:31:31 PM -03 2022>
#+description: What does it mean to be bold in God's presence?
#+filetags: boldness missionaries persecution Jews genties courage Lystra Derbe Antioch Perga Attalia prayer signs wonders Paul Barnabas apostles Jupiter Mercurius
#+options: toc:2 num:nil ^:nil

#+begin_preview
What does it mean to be bold men or women in God's view?

Courage and boldness is a totally different subject in God's view and has nothing to do with the worldly Macho view the System teach us...

So that, lets analyse the issue based on Acts 14
#+end_preview

* Bible text

Acts 14:1: Now at Iconium they entered together into the Jewish synagogue and spoke in such a way that a great number of both Jews and Greeks believed.

Acts 14:2: But the unbelieving Jews stirred up the Gentiles and poisoned their minds against the brothers.

Acts 14:3: So they remained for a long time, speaking boldly[fn:1] for the Lord, who bore witness to the word of his grace, granting signs and wonders to be done by their hands.

Acts 14:4: But the people of the city were divided; some sided with the Jews and some with the apostles.

Acts 14:5: When an attempt was made by both Gentiles and Jews, with their rulers, to mistreat them and to stone them,

Acts 14:6: they learned of it and fled to Lystra and Derbe, cities of Lycaonia, and to the surrounding country,

Acts 14:7: and there they continued to preach the gospel.

Acts 14:8: Now at Lystra there was a man sitting who could not use his feet. He was crippled from birth and had never walked.

Acts 14:9: He listened to Paul speaking. And Paul, looking intently at him and seeing that he had faith to be made well,

Acts 14:10: said in a loud voice, Stand upright on your feet. And he sprang up and began walking.

Acts 14:11: And when the crowds saw what Paul had done, they lifted up their voices, saying in Lycaonian, The gods have come down to us in the likeness of men!

Acts 14:12: Barnabas they called Zeus, and Paul, Hermes, because he was the chief speaker.

Acts 14:13: And the priest of Zeus, whose temple was at the entrance to the city, brought oxen and garlands to the gates and wanted to offer sacrifice with the crowds.

Acts 14:14: But when the apostles Barnabas and Paul heard of it, they tore their garments and rushed out into the crowd, crying out,

Acts 14:15: Men, why are you doing these things? We also are men, of like nature with you, and we bring you good news, that you should turn from these vain things to a living *God*, who made the heaven and the earth and the sea and all that is in them.

Acts 14:16: In past generations he allowed all the nations to walk in their own ways.

Acts 14:17: Yet he did not leave himself without witness, for he did good by giving you rains from heaven and fruitful seasons, satisfying your hearts with food and gladness.

Acts 14:18: Even with these words they scarcely restrained the people from offering sacrifice to them.

Acts 14:19: But Jews came from Antioch and Iconium, and having persuaded the crowds, they stoned Paul and dragged him out of the city, supposing that he was dead.

Acts 14:20: But when the disciples gathered about him, he rose up and entered the city, and on the next day he went on with Barnabas to Derbe.

Acts 14:21: When they had preached the gospel to that city and had made many disciples, they returned to Lystra and to Iconium and to Antioch,

Acts 14:22: strengthening the souls of the disciples, encouraging them to continue in the faith, and saying that through many tribulations we must enter the kingdom of *God*.

Acts 14:23: And when they had appointed elders for them in every church, with prayer and fasting they committed them to the Lord in whom they had believed.

Acts 14:24: Then they passed through Pisidia and came to Pamphylia.

Acts 14:25: And when they had spoken the word in Perga, they went down to Attalia,

Acts 14:26: and from there they sailed to Antioch, where they had been commended to the grace of *God* for the work that they had fulfilled.

Acts 14:27: And when they arrived and gathered the church together, they declared all that *God* had done with them, and how he had opened a door of faith to the Gentiles.

Acts 14:28: And they remained no little time with the disciples. (ESV2011)

** Introduction

Our society, not only in a national way, but in a international level, teach us that manhood is about, above of all, strong virility, getting as many women as possible, drinking, partying and living a lifestyle of freedom.

Freedom here means doing whatever one wants, irrespective of personal and social consequences.

These concepts come from the domineering Macho and Patriaral view of the world...

This is a sign of being real men in the view of a sinful and rebellious society that does not know *God* and His *holly Word*

*Jesus* was a fully man, although He had no women, had no children and did not get married throughout His whole life...

*He* was the boldest man in the world.

*His* battles were raised not only in the natural world through fierce arguments when challenged by the religious authorities, but above of all, such wars happened mostly in the spiritual realm.

For that, we can see that what the *Lord* sees as courage, manhood and boldness is way beyond what most people, including many Christians do...

This text show us a glimpse of what is to be a bold, courageous,

brave and real men, and women, in *God*'s eyes.

So that, let us have a taste of it.
  
** Development

*** Bold in the middle of an unnatural war - part 01

Those men were very bold, we can see it through their attitude of going into a synagogue and preaching the true Gospel of *God* to the Jews as well as to gentiles.

The text says that they preached a long time and "*boldly*[fn:1] in the *Lord*.

Now, the simple fact of going into a synagogue to give testimony about Jesus was not without risk and the two apostles knew that very well.

It was to enter into an enemy realm. And that held, by itself, a big challenge.

They knew that they have to face not only the Jews who used to reject their message and the battle would extrapolate the human level and reach the spiritual level.

This attitude stirred up all the demonic forces around and those bad guys - the daemons - took over the Jews' minds from those who did not believe in Christ and persuaded them to persecute and stone Paul, Barnabas and the other brothers in such a way that they had to flee to other towns - Lystra and Derbe

This move resulted in many Jews and gentiles being converted to Jesus Christ.

But, why some Jews and gentiles were converted? Because of the boldness of Paul and Barnabas and other faithful Christians who were with them?

What is your view here? I think that verse 3 can answer this question:

Acts 14:3: So they remained for a long time, speaking boldly[fn:1] for the Lord, who bore witness to the word of his grace, granting signs and wonders to be done by their hands.

The *Lord* was so happy with them that their teaching and preaching were backed up by *Him Who* made them to perform many signs and wonders through their hands.

Teaching and preaching like they did requires a great deal of confidence in the *Lord*.

Confidence that *He* would be there with them.

And *He* was indeed... The apostles and brothers could nearly touch *Jesus* presence in their midst.

The *Lord* told them, a little while before going back *Home*, *He* would be in, a special manner, with those who were engaged with the task of going and preaching the Gospel:

 Matthew 28:18: And Jesus came and said to them,  [ch. 11:27; Dan. 7:13, 14; John 3:35; 13:3; 17:2; Acts 2:36; Rom. 14:9; 1 Cor. 15:27; Eph. 1:10, 20-22; Phil. 2:9, 10; Col. 2:10; Heb. 1:2; 2:8; 1 Pet. 3:22; [ch. 9:6; John 5:27] All authority ch. 6:10; Luke 2:14] [ch. 6:10; Luke 2:14] in heaven and on earth has been given to me.


Matthew 28:19:  [Mark 16:15, 16] Go therefore and  [ch. 13:52] make disciples of  [Luke 24:47; [ch. 24:14; Mark 11:17; Rom. 1:5] all nations,  [Mark 16:15, 16] baptizing them  [See Acts 8:16] in [2 Cor. 13:14] the name of the Father and of the Son and of the Holy Spirit,

Matthew 28:20: teaching them  [John 14:15] to observe all that [Acts 1:2] I have commanded you. And behold, [ch. 1:23; 18:20; John 12:26; 14:3; 17:24; Acts 18:10] I am with you always, to  [See ch. 13:39] the end of the age. (ESV2011)

So, here, we see the *Lord* fulfilling *His* promise and applying *His* authority to make them to perform those signs confirming their words.

We, as Christians, say that the *Lord* is with us wherever we are and go and it is true. But, *Jesus*' presence in power is a specific promise for those who are totally engaged into obeying *Him* in the sense of preaching and teaching *His* holly Word and facing superhuman challenges.


If you want to see *God* acting in supernatural way through your hands, just prepare yourself to obey the *Lord*, engage in the noble task of teaching and preaching the Gospel of salvation, and trusting that *He* will show up, in a special way, to back up your work.

*Jesus* power in not meant to be a TV show at the will of many televangelists and those who try to lure people for their own gain, above of all, financial one.


If someone is "performing" signs and wonders just to impress you, your family, your church, your community, your estate, your country, and is not engaged in teaching and preaching the Word of *God* as it should: look! Be aware not to be deceived by such person, such team, such church, such denomination, such Christian organisation, et.al...

The *Lord*, Himself, never performed a sigle sign to just satisfy peoples desires and curiosity.

Once the Jews came to *Him* asking *Him* to perform a miracle...

Did the *Lord* do that?

Can you find that text?

Oh! It is here:


Matthew 16:1:  [For ver. 1-12, see Mark 8:11-21] And the Pharisees and Sadducees came, and  [John 8:6] to test him  [1 Cor. 1:22; See ch. 12:38] they asked him to show them  [Luke 11:16; 21:11] a sign from heaven.

Matthew 16:2: He answered them, [Luke 12:54, 55] When it is evening, you say, It will be fair weather, for the sky is red.

Matthew 16:3: And in the morning, It will be stormy today, for the sky is red and threatening. [Luke 12:56] You know how to interpret the appearance of the sky, but you cannot interpret [ch. 12:28; Luke 19:44] the signs of the times.

Matthew 16:4:  [See ch. 12:39] An evil and adulterous generation seeks for a sign, but no sign will be given to it except the sign of Jonah. So  [ch. 4:13; 21:17] he left them and departed. (ESV2011)

Why do you think *Jesus* did not perform the sign the Jews wanted *Him* to do?

Firstly, because *Jesus* obey not any men. For *He* is the *Lord* of us all; secondly, because, that request was a temptation to *God*. And was just an attempt to fool the *Lord* for making *Him* a showman and *He* didn't came to this earth to exhibit *His* power before *His* own creatures.

*He* came here to give us the most precious opportunity we can have throughout all our life: to make a decision of how we want to live this life in relation to *God* and, particularly, the after life...

*He* came here to call us into a relationship of life (eternal) through *His* work, sacrifice, death and resurrection.

So that, we either take it and follow *Him*, or reject it and follow our own way which will lead us into eternal perdition...

The multitude of the city got divided under Paul's and Barnabas preaching. Some where for and others against the apostles and brothers.

No wonder that those who heard the message preached got divided. After all, they were right in the middle of a battlefield.

So that, division is a common ground when it comes to raising wars.

Sword can be a symbol of wars. Especially in those days when the roman empire was in power.

The *Lord* told this when still on this earth:

Matthew 10:34: Think not that I am come to send peace on earth: I came not to send peace, but a sword . (KJVA)

What! Did *Jesus* come to earth to mess it even more with wars?

Not at all! Dividing is the effect of teaching *His* Holly Word.

Countries can be divided by it, as it happened in the Reformation period; Christian organisations can be divided over it; families members can also be divided upon hearing and accepting the *Gospel* of salvation...

Matthew 10:35: For I am come to set a man at variance against his father, and the daughter against her mother, and the daughter in law against her mother in law. (KJVA)

Matthew 10:36: And a man's foes shall be they of his own household. (KJVA)

So that, those who preach and teach it will obtain many enemies along their journey...

You have to be bold to get fully engaged in a war. don't you?


*** Bold in the middle of an unnatural war - part 02

After fleeing to Lystra and Derbe they kept preaching the Gospel

In Lystra, Paul came across a crippled man. or rather, the other way round. Upon listening to Paul, the apostle could see that he had faith to be healed, so that he prayed for him and he was healed.

Because of that tremendous miracle, the lycaonic people wanted to sacrifice to Paul (Mercurius) and Barnabas (Jupiter). But they were convinced of not doing so

In verse 15, we see Paul, Barnabas and the apostles preaching a very straight message to the people who wanted to sacrifice to them.

They clearly told them to covert to *God* from their vanities and showed them the only true *God* who created everything.

We could well say that on part 01 it was not so clear that the whole thing involved an spiritual battle at first.

But, here, in Lystra, we can see it very clearly that the spiritual realms are playing the main part behind the scenes...

Paul tells them about how good *God* is. The *One* he is telling them about.

The testimony of his goodness is such that the own Nature should be enough to bear witness of *Him*: the rain that falls down on our plantations, the beautiful seasons, the fruits, vegetables, cereals and every kind of food the land can provide us.

And on top of all of this, He also gives us joy. What a *God*!

Only blinds and stoned hearts and the /New Atheists/[fn:2] disciples cannot see *His* existence in all of this.

Paul's and Barnabas teaching made all the regional daemons to be even angrier at them.

So that, some Jews from Antioch and Iconium arrived at the town to physically attack them in such a way that Paul was stoned and drawn out of the city as a dead man.

Be warned that if you push to teach the *Gospel* some will welcome the amazing Truth, but you can stir up the celestial regions and be the  target by those hellish spirits.

We saw there were an attempt to physically kill the apostles when they were in Iconium. But, fortunately, the conspiracy was found and they flew away from that city.

But, the hellish guys were not happy and just sent his servants to try to kill the missionaries once more.

This is exactly how the devil acts: he tries to convince people through his twisted arguments, mostly of them are subtle fallacies; them if the person is smarter than him in *God*'s Word, he shows himself not as an "angel of light", but a terrible killer...

He has been using this technique during the whole history of humankind.

Which was the two most classical examples in the Bible?

Hint: they are in the book of Genesis and in the Gospels...

Yeah! That's it:

Firstly when he persuaded the woman through his cunning bent words, taking *God*'s Holly words and convincing her about a hidden knowledge she would get if she did exactly the contrary of what the *Lord* had told us not to do...

This time, he came as an enticing beautiful and seducing creature.

So, there were no need to show his ugly and murderer face...

Secondly, he tried his old technique of twisting *God*'s Word with *Jesus Christ*.

But, now his millennial distorted Psycho-Theology couldn't do any harm to our *Lord* who is the Truth *Himself*.

By the end of that Psycho-Theological-Intellectual-spiritual battle, he had to go away as a looser.

But, then he kept trying to get the *Son of Men* with lesser battles of the type.

When he convinced himself he was not up to our *Lord*, he took the physical and violent method as the last option against the *Lord*.

He did not know that killing *Jesus* would the final strike he would get straight on his head... So that, he was once and for all done by the death and Resurrection of our *Saviour*.

Yes! Be warned when taking the noble task of teaching and preaching *God*'s Word. The devil will play dirt, if he cannot stop you by using lighter tricks.

But, be courageous and brave because you hold the holly Sword and *Jesus* the *One* who already won over the prince of darkness is fighting by your side and *He* will never, but never, let you down...

Just keep on obeying and trusting in *Him*.

As Paul, Barnabas and the early Church did...

To our very surprise, after all this upheaval, they went back to the very cities they first were persecuted: Lystra and Iconium and reached his missionary church in Antioch

They did this to check the souls up who had been converted and were left on their own after they first left those cities. This really is what *Jesus* told to his disciples to do: preaching, teaching and shepherding the flock.

We cannot claim that we've done *God*'s work in full, if we only preach, only teach and just go on our way, letting people behind without giving assistance to them.

Preaching and teaching the Gospel is a serious task and not an adventure we do when we have free time...

This is not a discourages advice I'm giving you. But, I am only trying to recall, first and foremost, myself.

It is OK to share, as people sometimes call, Christian literature to the unbelievers. But, just sharing them and telling them: *Jesus* loves you is not evangelising them in a biblical way...

Of, course, there are exceptions to the rule, e.g., if *God* places this desire into your heart and your have no time and no opportunity to engage in a 1-to-1 deeper conversation with those who you are trying to lead to Christ, I think, the *Lord* is wise more than enough to turn any Christian literature, into a powerful weapon to reach the open hearts.

But, it is not the rule...

And Paul and Barnabas as well as the whole book of Acts (of the *Holy Spirit*) through the Apostles teach us about the right way to fulfil *Jesus*' commission through our lives on earth.

We've set to confirm the faith of those who have received *Jesus Christ* in their lives through our hands as instruments of the power of the *Holy Ghost* in us, and, above of all, they have to feel that we truly love, and care for them. After all, love is what do make a difference in our ministry towards people.

In the case of Paul and Barnabas, they only left them after establishing a new church there and choosing elders to care for them, believing that *God* was more than enough to keep for their lives and make them grow. After that, they still preached in Perga on their way back to Antioch

What a lesson for us who live in a said to be /Liquid society/?

In a burn out society! Where we are driven to show work in 24/24 routine to the point of killing ourselves till exhaustion![fn:3]

** Conclusion

Questions:

How we Christians are used to deal with other peoples' culture and religion?

Are we cowards under the guise of being politically correct in such a way that we could never be bold enough to engage in Theological debates and preach the real Gospel of *Jesus Christ*?

Why you and I do not do that? Is this because we were taught to leave this hard task to the "professional" of Faith? (pastors, ministers, missionaries, Bible teachers and the like?)

Or is it because we are lazy to study *God*'s Word and we feel unprepared to preach a more apologetic Gospel?

Have you been challenged in your workplace, school, college, university, or among your family to expose the reason on your faith and your did no do well and smart people squared you in a way you felt really bad?

What if you had the will and acted beforehand building up your faith and preparing yourself face such situations?

You would like to... But, does not know where and how to start...

So, I leave here an advice to you: ask your leader in your church to prepare a course on Apologetic to prepare you and those who want to fulfil the Great Commission in a serious and engaged way...

And, most important, pray to *God* while you build your faith to be a faithful testimony of *His* Word.

You say that you have not bear witness using that methodology because we have to respect other people's religion and cultural background?

We see that Paul and Barnabas made a very challenging and bold move on this occasion. For them, saving lives and sowing the Word was worth of taking the risk. Even being aware that their lives were at risk...

We have to be prepared to face opposition whenever we open our mouths with the true Gospel of *Jesus Christ*. When we do that, for sure, daemons, and the whole hell will stir up against us and try to use his servants' minds against anyone who is bold to hold fast *God*'s Word. Irrespective of being "professional" preacher, missionaries, church-goers, or whomever...

In a way that, if possible, the hellish gang will try to use every kind of dirty devices in an attempt to make *God*'s children to shut their mounts up forever in this life.

But, we have to stand firm and trust that *God* is the *One* who is with us when on the move making *His* will.

Even if we have to face stones coming in our direction to hitting our faces and body, *God* can well resurrect out bodies by *His* sovereignty will. In case we've not yet fulfilled *His* whole plans in us and through us [fn:4][fn:5].

In a way, even if we do not know the level of Paul's wounds and pain, this was what *God* did in that occasion: *He* preserved his life and his companions, so that they could fulfil their calling for their generation...

All that *boldness on behalf of *God*'s name had a high price to be paid, but the other side of the coin is much more special...

The fact that they could see *God* acting through their teaching and preaching was way more rewarding than the fear of being persecuted and even murdered!

I am sure, that apart from feeling the *Lord* acting through them, they felt *His* presence is a strong way, as to build up their faith in *Him* and confidence in the Message of the Gospel which is pure power to set men free from the bondage of sin and break every chain[fn:6] pushing them into Hell...

Why we do not see *God* acting in this way - with signs and wonders - anymore?

Is there something with *God*? Has *He* changed or is *His* Word just a bunch of fictional stories?

What do you think?

I think, IMHO, that we've not seen the *Lord* acting through signs, power and wonders, because of our timidness to raise *His* name on high in our life and wherever we are.

Not that we have to act in an aggressive and uncaring way.

Because, boldness has nothing to do with arrogance and lack of wisdom.

Remember Moses that was a meek man, but powerful and bold for doing *God*'s will.

He was able to face and challenge one of the most powerful man in his generation: Pharaoh Ramses II[fn:7]

We want *God* to revive our churches, denominations, and country, but we want it our way...

Meaning that *God* will operate wonders inside our churches building during our services, while we are all comfortable sat down in our cushion chairs or benches.

In reality we want to take part in a *show* where it showcase *God* to others.

Specially these days, where our churches are surrounded by cameras everywhere, outside and inside our well built temples.

No matter who is delivering the Word of *God*.

So that, children of the *Almighty* who are preaching the *Word* in persecuted places and countries have to warn the leaders of  such churches that they should not record them sharing what *God* has been operating in such places.

Unfortunately, some church leaders have never put their feet in an hostile missionary field and are totally unprepared to even understand a simple request like this one from a person who is involved in such ministries

I get amazed on how most Bible Colleges and Theological organisations are preparing the young generation to lead their churches and Christian organisations nowadays...

But, I will leave this issue to discuss another time...

Otherwise, we will be off-track here...

Certainly, the *Lord* does not act as *He* did in the pages of the Bible, not because *He* has changed. But, because we read *Him* in a slightly wrong way.

So that, it is a good exercise, from time to time, try to check the god we sometimes start building with the real *One* the Bible tell us...

I believe that we do not see that powerful *God* the Bible tell us about because, lets be fair with us here, we are, most of time, a bunch of fearful Christians who do not really believe in *God* and fully understand what *He* is able to do. presence and assistance while we are teaching and preaching.

Apart from that, as I already have stated above, our mission is mostly restrict to our internal and safe well built buildings.

While the Mission *Jesus* told us to do is totally the contrary:

Matthew 10:16: Behold, I send you forth as sheep in the midst of wolves: be ye therefore wise as serpents, and harmless as doves.

Matthew 10:17: But beware of men: for they will deliver you up to the councils, and they will scourge you in their synagogues;

Matthew 10:18: And ye shall be brought before governors and kings for my sake, for a testimony against them and the Gentiles.

Matthew 10:19: But when they deliver you up, take no thought how or what ye shall speak: for it shall be given you in that same hour what ye shall speak.

Matthew 10:20: For it is not ye that speak, but the Spirit of your Father which speaketh in you.

Matthew 10:21: And the brother shall deliver up the brother to death, and the father the child: and the children shall rise up against their parents, and cause them to be put to death.

Matthew 10:22: And ye shall be hated of all men for my name's sake: but he that endureth to the end shall be saved.

Matthew 10:23: But when they persecute you in this city, flee ye into another: for verily I say unto you, Ye shall not have gone over the cities of Israel, till the Son of man be come.

Matthew 10:24: The disciple is not above his master, nor the servant above his lord.

Matthew 10:25: It is enough for the disciple that he be as his master, and the servant as his lord. If they have called the master of the house Beelzebub, how much more shall they call them of his household?

Matthew 10:26: Fear them not therefore: for there is nothing covered, that shall not be revealed; and hid, that shall not be known.

Matthew 10:27: What I tell you in darkness, that speak ye in light: and what ye hear in the ear, that preach ye upon the housetops.

Matthew 10:28: And fear not them which kill the body, but are not able to kill the soul: but rather fear him which is able to destroy both soul and body in hell.

Matthew 10:29: Are not two sparrows sold for a farthing? and one of them shall not fall on the ground without your Father.

Matthew 10:30: But the very hairs of your head are all numbered.

Matthew 10:31: Fear ye not therefore, ye are of more value than many sparrows.

Matthew 10:32: Whosoever therefore shall confess me before men, him will I confess also before my Father which is in heaven.

Matthew 10:33: But whosoever shall deny me before men, him will I also deny before my Father which is in heaven.

Matthew 10:34: Think not that I am come to send peace on earth: I came not to send peace, but a sword. (KJVA)

In sum: *He* sent us into a battlefield and there is no way to get out of it...

All of us, who are true children of *God* are involved in this war!

So that, it is not only a matter of preaching, teaching, praying, and interceding in a safe place - inside our churches and Christian organisations - but doing all of  this in hostile places and environments.

This is the key for *God* being with us in the power of the *Holy Ghost* as *Jesus* promised his disciples before going up home.

*He* told them that *He* would be with them in a very especial way whenever they were at work, believing in *Him*, trusting in *Him*, preaching and teaching *His* Word till the edge of the earth.

Please do not ask me if I believe in the flat-earth theory... Edge here is just a imagery and literary resource...

Another predictable behaviour that they certainly knew would happen was that their audience at some point would be divided and should have to take sides on *God*'s message of salvation.

Jesus Himself told that people even inside their own families would be divided under the Gospel.

*God*'s message is a message that any men cannot be neuter on hearing it.

Every man have to decide which side to take when faced with the /Message of life/.

This /Message/ is not an unimportant story that happened in the first century, but it is *the most important News* a men can hear.

Because the decision over this Message will decide our eternal future and there is no way out of this very truth.

They could well be intimidate about what had just happened in Iconium when they reached Lystra and Derbe.

But, far from true, they kept preaching the Gospel in those cities as well.

This teach us the *level of commitment* they had with the Message of Jesus Christ.

In fact, the only thing that stopped those first brothers of us was to be put to death.

Anything else didn't work against them - The cure of the crippled man tell us that sometimes the necessary faith for someone to be healed is not on the one who prays for, but, rather, on the person who receive the prayer.

How different from Paul's and Barnabas' behaviour some churches/denomination VIP leaders act nowadays!

Paul and Barnabas did not want any glory to themselves, but *God*'s.

We are already kinda of fed up on seeing "big stars" in these Institutions getting *God*'s glory on their shoulders as if churches, denominations and Christian organisations they lead have its growth root in their own "superpowers" instead of *God*'s mercy and grace.

We have to immediately, repent before *God* as a true church and avoid any kind of adoration to men and, even more, to ask the *Lord* to clean these figures away from ministry if they do not repent of their behaviour of being worshipped as if they are gods.

The Church has been too tolerant with these kind inside her.

It is time to do something to stop these people who mislead *God*'s flock to the eternal abyss.

I am, of course, not advocating and kind of physical violence against anyone. But, if we pray insistently to our *God*, *He* can do something new...

We should say: but, this is the way the Bible already foretold us, so let it be...

I do not think that the *Message* is passively conniving with such leaders among *God*'s people.

At least, I see not it in the epistles of the New Testament.

I see men of *God* who dealt with wrong teaching and cunning spiritual leaders inside the churches...

When I read this text, I wonder how have been our presentation of the Gospel of *Jesus Christ* an the true *God* to other people.

We seem to be so shy in our way of approaching people.

We do not challenge them, we do not tell them the real truth about who *God* is and what they should do to be forgiven by *Him* and enter his Kingdom.

We have to change our way of preaching and teaching the *Gospel*.

There is no time to be circumventing the true message of *God*.

We have to go straight to the point without fearing any kind o retaliation.

We should be prepared to face the most ferocious spiritual battles whenever we start preaching and teaching the Gospel in the devil strongholds like those cities.

Demonic forces will certainly raise up and use people, especially religious people - Jews, pagans and Christians alike - to try to stop us at any cost.

After the WWII Zionists have coined the term "Antisemitism" to fool everyone who do not agree with the Zionist project.

If you criticise Modern Israel or Jews anywhere in the world you are labelled as being antisemitic.

But let us not forget that in the early days of Christianity and even today, one of the most fierce enemies of Christianity are the Jews.

Through the pages of Acts (of the *Holy Spirit*) through the Apostles, we can clearly see this pattern over and over again.

But, praise the *Lord*, because many Jews got converted to Jesus Christ back then and still are turning to *Christ*.

Any many others either Jews and gentiles will fill up the True Church, the Bride of Christ, through the bold and courageous preaching and teaching of the true *Message* till the day the Almighty *Lord* comes to reign for ever and ever.

While this day does no come, we should go on believing, trusting and obeying the *Lord*.
*God* bless you all.


Sun Sep 11 09:52:30 PM -03 2022

Cypher

** Footnotes

[fn:1]
speaking boldly παρρησιαζόμενοι (parrēsiazomenoi) Verb - Present Participle Middle or Passive - Nominative Masculine Plural Strong's 3955:   To speak freely, boldly; To be confident.

[fn:2] [[https://www.theguardian.com/books/2019/jan/31/four-horsemen-review-what-happened-to-new-atheism-dawkins-hitchens
\[p\]     ][The New Atheism and the Four Horsemen]]

[fn:3] [[https://yewtu.be/watch?v=_qikrYBd4tw
][The Burnout Society]]

[fn:4] [[https://en.wikipedia.org/wiki/Richard_Wurmbrand][Richard Wurmbrand]]

[fn:5]

As citações apontando para artigos da "Wikipedia" não são autoritativas do ponto de vista acadêmico. Servem, somente, como ponto de partida para pesquisas mais aprofundadas e de cunho científicas.

The citations pointing to Wikipedia articles are not authoritative from an academic point of view. They only serve as a starting point for more in-depth and scientific research.

[fn:6][[https://invidio.xamh.de/watch?v=EtyVdC7E6Wo][ Break Every Chain]]

[fn:7] [[https://www.britannica.com/biography/Moses-Hebrew-prophet/Moses-and-Pharaoh][Ramses II]]
