#+title: Eden 2.0
#+date: <2022-08-31 12:01>
#+description: The New Garden of Eden Already Prepared for Us - O Novo Jardim do Éden já Preparado para Nós
#+options: toc:2 num:nil ^:nil
#+filetags: Eden Éden Garden Jardim Creation Criação

#+begin_preview

O que aconteceu com o Jardim do Éden?

What happened with the Garden of Eden?

#+end_preview

* Éden Versão 2.0

** Atualizado por Deus

Genesis 2:8: Então plantou o *Senhor* Deus um jardim, da banda do oriente, no Éden; e pôs ali o homem que tinha formado.

Genesis 2:9: E o *Senhor* *Deus* fez brotar da terra toda qualidade de árvores agradáveis à vista e boas para comida, bem como a árvore da vida no meio do jardim, e a árvore do conhecimento do bem e do mal. (PorAR)

Estes são versículos maravilhosos...

Porque nos diz que depois que o *Senhor* criou a maior parte de sua Obra, através do infinito poder da Sua Palavra... O senhor, Nosso *Deus*, fez um Jardim "à mão", todo especial, e o deu como Presente ao homem e à mulher que Ele mesmo acabara de criar!

Ele fez isso de forma tão cuidadosa que o lugar era (e é) realmente uma Obra-prima de Suas mãos.

Podemos imaginar que o processo foi parecido como se Ele tivesse "arregaçado as mangas da camisa", retirado as sementes de um saco, aberto as covas na terra Santa, e as plantado de forma minuciosa...

E para a alegria dos Naturalistas: Ele não usou nenhum tipo de pesticida para fazer com que as árvores e a vegetação do Seu perfeito Jardim crescessem e desbrochassem.

Além do mais, elas não eram apenas úteis para comer, mas para contemplar a própria beleza e poder do *Senhor* estampados nos troncos, galhos, folhas e frutos...

Árvores dignas de contemplação e admiração... Mas não de adoração...

A Palavra de *Deus*, por exemplo, repetidamente, nos fala, da beleza, esplendor e robustez dos cedros do Líbano. E nos faz pensar no poder, força e beleza do próprio Criador.


Psalms 29:5: A voz do *Senhor* quebra os cedros; sim, o *Senhor* quebra os cedros do Líbano. (PorAR)

Bem... Alguns poderiam dizer que o Jardim do Éden era um lugar simbólico que Moisés, no livro de Gênesis, nos relata... Nesse caso, também deveríamos considerar que nós, como homens e mulheres, também somos criação simbólica, ou virtual, de *Deus* e que, no final das contas, vivemos em uma grande Matrix, como a famosa série de filmes Matrix nos faz refletir...

Mas, por que o "Éden simbólico" seria um tema que, indiretamente, se repete no livro que encerra a profecia Bíblica, Apocalipse?

E também porque a Árvore da Vida é recorrente naquele livro?

Nesse sentido, a Nova Criação também seria uma nova criação Virtual de *Deus*? E, de fato, estamos vivendo um grande sonho dentro de uma Matrix? Se isso for verdade, então Hegel e a escola filosófica do Idealismo, baseada no Platonismo, estão totalmente corretos e a Bíblia é um livro cheio de estórias fictícias, pois não segue a linha do Realismo filosófico! E, para complicar mais a situação, nós, seres humanos somos auto-enganados por um *Deus* que também é fruto da nossa imaginação sendo que tal imaginação também não é real porque ela mesma é fictícia...

Enfim... tais pensamentos são dignos de serem levados em consideração...

Voltando à terra novamente...

Será que perdemos, para sempre, aquele lugar tão lindo e perfeito que deveríamos, de fato, ter desfrutado?

Mas não foi possível por causa dos pecados de nossos antepassados e dos nossos próprios?

Poderíamos terminar esta breve reflexão com a triste nota de: "tudo agora está perdido, tudo está perdido..."

Mas, como Milton, em seu poema épico "Paraíso Perdido[fn:1][fn:2]", Jesus se ofereceu em sacrifício vivo para nos resgatar...

Felizmente, nosso bondoso e amoroso *Deus* tinha um Plano perfeito, por meio de Jesus Cristo, Seu Filho unigênito, para nos salvar, resgatar a Natureza, também contaminada pelo pecado, e, finalmente, nos levar para um outro lugar incrível, ou, talvez, o mesmo lugar...

A pergunta aqui seria: Por o *Senhor* colocou anjos (querubins), a leste do Jardim, para guardar o caminho de acesso até a Árvore da Vida depois da Queda do homem?

Sabemos, pela lógica e pela Teologia, que Ele o fez para que o homem não comesse do fruto da Árvore da Vida após ter pecado contra o *Senhor*. Pois, provavelmente, as consequências seriam bem maiores para eles e para nós...

Mas, além de fechar o acesso do lado oriental do Jardim, o seu acesso total também foi impedido. Hoje, temos somente uma ideia da geografia do lugar. Mas, depois que Adão e Eva foram expulsos de lá não se tem notícias de que algum homem tenha encontrado e  visitado aquele lugar tal como era no passado...

Teria o *Senhor* a intenção de revelar o Edén numa versão atualizada para os santos e santificados em Cristo no final dos Tempos?

Mas, o Tempo tem fim? Se teve fim, também teve começo... Porque somente o *Senhor* não teve nem começo e nem terá fim... Mas, esse é um assunto para outra ocasião...

Podemos, talvez, dizer que uma versão 2.0 do Éden nos espera na Eternidade?

Um lugar onde todos os filhos e filhas de *Deus* viverão juntos, para sempre, em perfeita harmonia com a Divindade (Trindade), a Natureza e os anjos de *Deus*...

Eu realmente espero estar lá, e te ver lá também, e a todos aqueles que em todos os lugares invocam, amam e servem nosso *Senhor* e Salvador Jesus Cristo.

Aquele lugar, extraordinário, vai ser aqui e ali. O profeta Isaías nos dá uma prévia daquele lugar, que, supostamente, será aqui na terra, e o apóstolo João nos mostra, sem sombra de dúvidas, a versão final dele, tanto a terrena quanto a celestial (Apocalipse 21:1-6).

Significando, que todo o Universo, toda a criação de *Deus* que foi contaminada pelo pecado, no final das contas, será totalmente refeita pelo poder infinito do nosso Criador e Pai...

E assim estaremos eternamente com Ele...

De uma coisa tenho certeza: Ali, e aqui, é o nosso verdadeiro Lar...

Meu sogro, poucas semanas antes de partir, com Parkinsonismo agudo, repetia essa frase, constantemente: "Quero  ir pra Casa..."

E eu, hoje, digo: e nós também...

Que o *Senhor* nos abençoe a todos...

VRFdS

Tue Aug 30 06:37:13 PM -03 2022

* Eden Version 2.0

** Upgraded by God

Genesis 2:8: And the LORD *God* planted a garden eastward in Eden; and there he put the man whom he had formed.

Genesis 2:9: And out of the ground made the LORD *God* to grow every tree that is pleasant to the sight, and good for food; the tree of life also in the midst of the garden, and the tree of knowledge of good and evil. (KJVA)

These are amazing verses...

Because they tell us that: *God*, apart from creating most of His Creation by just telling them to be through the power of his Word, in the case of the Garden of Eden, they say that this place was *God*'s handmade.

That was a special gift from *God* to the men[fn:3] whom he had just created! He did that in such a marvellous way that we can justly state the Garden was really a *God*'s Masterpiece.

We should notice that the whole process of crafting that was as if *God* had rolled up his sleeves, taken His seeds from a bag, opened the holes in the holly ground, and planted them in a careful way...

And for the adepts of Naturalism, best of all, He did not use any pesticide, as we would do, to make His vegetation and trees to fully grow.

By the way, they were not only useful to eat, but also to see its astonishing beauty! Which, in turn, reflected the very beauty and power of our *Lord* stamped in themselves

They were trees worth of contemplation and admiration. - But not of adoration, of course...

*God*'s Word repeatedly, for example, tell us about the beauty, splendor and power of a special tree in Bible times: the cedars of the Lebanon. And it make us thing about the power and beauty of our *God*.

Psalms 29:5: The voice of the LORD breaketh the cedars; yea, the LORD breaketh the cedars of Lebanon. (KJVA)

Well... Some could say that the Garden of Eden was a symbolic place that Moses wrote about in the book of Genesis.

In that case, we should also consider that we, as men and women, are also a symbolic or a virtual creation of *God* and that, in the end, we do live in a big Matrix, as the famous Matrix film series had told us so a little while ago...

But, why the symbolic Eden would be a recurrent theme in the closing book of the Bible, Revelation?

And also: Why does the Tree of Life is repeated a theme in the same book?

In that sense, the New Creation would also be a new virtual creation of *God*? If it is so, are we living an extended dream into a Matrix? If all this is true, then Hegel and the Idealist school of Philosophy, which has its base on Plato (and the Platonism) are totally relevant and, it turns out, the Bible is a book full of fictional stories and characters which has got only a literary value. Because it does not tend the Philosophic Realism! And, to make things worse, we, human beings are a bunch of self-deceived virtual specie created by a *God* that is also a product of our deceived mind. Going even deeper, our own imagination is not a real one because, in itself, it is a fiction...

Anyways, let's not go off-road here...

But, such thoughts can arise when we consider the whole issue in a Theological and philosophical way...

Let's come back to the earth, though.

Anyways... There we have such a beautiful Place we should have enjoyed forever and ever.

But we couldn't because of our forefathers sins and ours own as well...

We could finish this brief reflection with this thought banging on our head during our short life on this earth: "everything is now lost, everything is now lost"...

But, as Milton wrote centuries ago, in his famous poem "Paradise Lost": Jesus was there, in the beginning, to offer Himself as a living sacrifice to rescue us from our own sins...

Another question we can think of, in this context, is: Why did the *Lord* ordained cherubim to guard the Orient of the Garden and to close the access to the Tree of Life - just after men's fall?

By reasoning and through studying Theology, we know that *God* did that to avoid the men to the fruit of that Tree after committing sin, because the consequences would be way too serious for them and for us...

Apart from closing the access to the Tree, it seems that all paths to the Garden were blocked. We, nowadays, have only a geographical hint about where that place was. But, after Adan and Eve were expelled from there we do not have any report of another man finding the place and going back to live there.

This situation make us raise a key question: Would the *Lord* have the intention to reveal a new and better version of the Eden in the End Times to His saints in Jesus Christ?

Did I mention the word time? But, has the time has an end? If so, it also had a beginning. Right? Because only our *Lord* no beginning and neither He will have an end... But, this is a discussion to be taken another time...

But, fortunately, our amazing and loving *God* had a Perfect Plan through Jesus Christ, His only begotten Son, to rescue us, rescue nature as a whole and take us to another dimension.

Could we think that an updated version of the Eden awaits for us in Eternity - say, a 2.0 Garden?

A place where all sons and daughters of *God* will be living together, forever and ever, in perfect harmony with the *Godhead*, the Nature and all His holly angels...

I really hope to be there and see you all there as well.

You who love (and keep on loving) and serve (and keep on serving) our *Lord* Jesus Christ, in that outstanding place which is going to be Here (the transformed Earth) and there (the Holy City).

The prophet Isaiah give us a preview of that place, that supposedly will be here on this very earth. And the apostle John, finally, give us, for sure, what will that place be. Both the earthly and the celestial versions (Revelation 21:1-6).

Meaning, the whole Universe, the whole Creation which were contaminated by sin will be totally redone, to a 2.0 version and up, by the infinite power of our *God* and Father...

And, thus, we will, eternally, be with the *Lord*...

Of one thing I am totally sure: that unspeakable place, as Paul saw and could not find words to describe, is our real Home... There and here.

My father-in-law, before passing away, when suffering of chronic Parkinsonism, used to repeat the phrase: "I want to go Home..."

And so do we all...

*God* Bless you my reader...

VRFdS

Tue Aug 30 06:37:13 PM -03 2022

* Footnotes

[fn:1] [[https://pt.wikipedia.org/wiki/Paraíso_Perdido][Paraíso Perdido]]

[fn:2]
As citações apontando para artigos da "Wikipedia" não são autoritativas do ponto de vista acadêmico. Servem, somente, como ponto de partida para pesquisas mais aprofundadas e de cunho científicas.

The citations pointing to Wikipedia articles are not authoritative from an academic point of view. They only serve as a starting point for more in-depth and scientific research.

[fn:3] Here it is a general term, meaning Adan, Eve and the whole humankind.
