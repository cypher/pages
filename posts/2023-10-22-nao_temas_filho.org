#+title: Nao_temas_filho
#+date: <2023-10-22 16:13>
#+description: Este é uma rápida mensagem acerca do perigo de temer ao homem e não a Deus
#+filetags: Não_temas filho temer Deus Jesus Cristo

#+begin_preview
Essa é uma breve mensagem que o Senhor deu-me em novembro de 2021. O
tema principal é sobre o Medo e porque devemos temer ao Senhor, e não
a homens.
#+end_preview

* Não temas, filho

- [[https://archive.org/details/do_not_fear_son][Vídeo aqui]]
