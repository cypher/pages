#+title: Entendendo o Salmo 82
#+date: <2023-01-14 15:12>
#+description: Estudemos o Salmo 82 ...
#+filetags: Salmo Salmo_82 deuses Deus Jesus Cristo justiça misericórdia julgamento suspensão_do_julgamento juízo juízo_de_valor Pirro Pirronismo cético ceticismo próximo inimigo
#+options: toc:2 num:nil ^:nil

#+begin_preview
O Salmo 82 é dirigido a uma classe especial de pessoas da nossa sociedade...
Contudo, apesar de ter uma mensagem clara e direta da parte do Senhor para tal grupo, tal palavra também é direcionada a cada um de nós "filhos do Deus Altíssimo" por adoção através do sacrifício perfeito do *N.S. Jesus Cristo*...
Portanto, vamos estudar a mensagem do Salmo?
O que acham?
#+end_preview

* Entendendo o Salmo 82

 - Salmo 82[fn:1]

** Introdução

- Queridos... Esse salmo nos fala a respeito do *dever de julgar as pessoas com justiça e retidão*...

- O Salmo é *dirigido* especialmente a uma classe de pessoas em posição de autoridade: *Os juízes*.

- Contudo, também /podemos considerá-lo em relação a nós mesmos/...

- /Há uma crença no meio cristão que não devemos julgar ninguém nem coisa alguma/.

- Na prática, *isso é impossível* pois estamos constantemente emitindo o que se chama: ="Juízo de valor"= (um /termo técnico da Filosofia/).

- Por exemplo: se formos atravessar uma rua, repleta de carros, temos de parar, olhar, analizar e considerar se devemos cruzá-la ou não; se formos a um supermercado, da mesma forma, temos de considerar se compraremos esse ou aquele produto; se tivermos de sair de casa, enquanto estiver "caindo um pé d'água" lá fora... também devemos averiguar a possibilidade de sair imediatamente ou esperar um pouco mais... e assim por diante.

** Desenvolvimento

- A crença de que qualquer tipo de julgamento que emitimos é intrinsecamente pecado contra *Deus* é "baseada" numa interpretação equivocada da Palavra de *Cristo* nos Evangelhos, especialmente na passagem em que *Ele* diz: "Não julgueis, para que não sejais julgados..."

- Contudo, o texto paralelo de Lucas, na maioria das vezes, é ignorado ou omitido[fn:2]... Pois diz também: "... não condeneis para não seres condenados[fn:3]..."

- O problema do julgamento humano é sempre a *parcialidade*. Naquele texto, o *Senhor* =não está condenando o ato de julgar em si mesmo=, mas /o ato de julgar sem conhecimento de causa e em seguida emitir um veredito de condenação/...

- Ou seja, _julgar de forma preconceituosa_, como observamos constantemente em nossa sociedade cheia de *preferências* e *racismos*...

- /"Preto é ladrão ou marginal..."; "Pobre aqui! Nesse aeroporto! Deve ser faxineiro"; "Barbudo! É muçulmano, terrorista e petista"; "Árabe! Então é terrorista e muçulmano..."; "Loira bonita! É burra"/, etc...

- Na realidade, o texto, nos mostra *exatamente o contrário*... Pois a *Palavra de Deus* nos diz que temos a capacidade de julgar, contudo, o julgamento que fazemos deve ser regado com =duas substâncias= essenciais:

- /Discernimento/:
- No mesmo texto, *Jesus* nos mostra essa verdade:

- "Não deis aos cães o que é santo, nem lanceis aos porcos as vossas pérolas, para não acontecer que as pisem com os pés e, voltando-se, vos despedacem."

- Minha pergunta aqui é: /Como podemos distinguir quem são os "cães"/, a quem o *Senhor* está se referindo, se não formos capazes de julgar com discernimento?

- O que acham? É possível?

- Então: Quem são esses cães?

- Se concordarmos com a *interpretação popular* de ="suspender todo tipo de julgamento"= (porque, intrinsecamente, se constitui em pecado contra *Deus*), será impossível obedecer a *Cristo* aqui. Porque, basicamente, estaremos colocando *Jesus* contra *Jesus* mesmo (pois haveria uma contradição no texto) e, da mesma forma, *Deus* contra *Jesus*, nesse capítulo da Bíblia. Percebem?

- Então seria total "*Balagan* - /bagunça em Hebraico moderno/".

- Mas, como podemos distinguir os cães dos não-cães?

- Que acham?

- Tenho uma dica:

- /Os cães são as pessoas que investimos em pregação e ensino da Palavra através de evangelização, estudos Bíblicos, orientação, conselho, oração/, etc... Contudo, eles *desconsideram totalmente a Palavra de Deus*, de forma prática e, muitas vezes, teórica, em suas vidas. Chegando até mesmo /a terem uma atitude de desprezo e zombaria com as coisas que a Palavra consideram santas/: o nome do *Senhor* e a *Bíblia*, os *mandamentos do Senhor*, o *Reino de Deus*, a *Salvação em Jesus Cristo*, et.al.

- Portanto, quando percebemos que =não querem ouvir, se arrependerem, buscar= o *Senhor* para que /sejam salvos e mudem o estilo pecaminoso de vida que têm/, *Cristo* nos diz para que não mais lancemos nossas =pérolas=[fn:4] e as =coisas santas= (a *Palavra de Deus*) a eles...

- Segue-se, assim, que é /impossível a interpretação popular/ de _"suspender todo o julgamento"_ atribuída à pessoa de *Jesus*...

- *Jesus* nunca pregou o *Pirronismo* e o *Ceticismo*[fn:5]

- Pelo contrário, o texto de *Mateus*, capítulo 7, é *muito mais acerca de como julgar do que de não julgar*...

- Pois vivemos numa época em que as *plataformas digitais*, sobretudo as chamadas "*Redes Sociais*" são verdadeiras =máquinas de engano=. Se não formos *cautelosos*[fn:6], poderemos ser controlados e manipulados de tal forma /pelo diabo e seus agentes/ por trás das Redes sociais que estaremos suscetíveis de cometer crimes hediondos por causa de conteúdos falsos e que se passam por verdadeiros em tais plataformas...

- Um *triste exemplo, dentre muitos, foi o linchamento de três pessoas por uma multidão*, /acusadas de sequestrar crianças, sem nenhuma prova,/ que aconteceu no México, em 2019, por causa de *uma acusação sem fundamento* que *"viralizou" no Facebook*[fn:7]

- Na realidade, o *Senhor* /"precisa"/ de *discípulos cheios do Espírito de Discernimento* para *influenciar, argumentar, e arrancar as massas* da =Fôrma desse Mundo=[fn:8] e /conduzir as pessoas (aqueles que você encontra por acaso, seus amigos, colegas de trabalho e familiares) do engano à Verdade e das trevas à Luz/ para que atrocidades (tanto físicas, quanto psicológicas) como essas sejam evitadas contra os inocentes...

- O texto, novamente, nos diz que devemos =discernir entre os verdadeiros profetas e os falsos profetas que vêm disfarçados de ovelhas=...

- É possível obedecer a *Cristo* nesse aspecto /sem julgar com grande discernimento/?

- Falsos =profetas disfarçados de ovelhas é tarefa dura julgar e discernir=... O próprio *Jesus* diz isso em outro lugar mais uma vez[fn:9]...

- *Misericórdia*:
- É a *outra substância* essencial que *Jesus* nos ensina no texto de Mateus 7...

- Quando *Ele* diz que /não devemos julgar para não sermos julgados/... está se referindo ao *tipo de julgamento humano* que, na maioria das vezes, _carece de misericórdia_...

- Então é aqui que devemos =atentar com precisão=...

- Quando emitimos um *juízo de valor* sobre qualquer pessoa, e o fazemos com o intuito de *condená-la*, pecamos gravemente contra o *Senhor*

- *Deus* é amor! Não devemos esquecer jamais. *Ele* tem mais /prazer em salvar do que em condenar/[fn:10]... E se quisermos ser a imagem e semelhança *dEle*, também temos que /refletir o caráter santo, justo e misericordioso/ *dEle*.

- Se julgarmos o nosso próximo =sem misericórdia=, ou seja, somente pelo =prazer de apontar o erro, envergonhar, abrir feridas já cicatrizadas que a vida tratou de fazer, "esmagar a cana quebrada e apagar toda centelha de esperança na vida dele(a)=[fn:11]", então seremos julgados pelo *Senhor* na mesma medida...

- O *Salmo em apreço*, também nos adverte sobre essa verdade. E _nem_ *os deuses* escaparão do *julgamento de Deus* (vv. 2-5)...

- E *quem são esses deuses* a quem o Salmo se refere?

- Ora, *são os juízes* =(aqueles que estão investidos de autoridade oficial e governamental para julgar as pessoas nas suas respectivas comarcas e nos seus diversos casos e processos)=

- Observem que o *Juiz Supremo* que está no meio *dos deuses (juízes)* é o *Senhor Jesus*

- E o que *Ele* está fazendo lá? *Ele* está com /os olhos e ouvidos bem abertos e atentos/, =observando a maneira de cada um julgar; e perscrutando o coração de cada juiz(a)=, todos os dias e em todos os lugares, quando os processos e outros casos são trazidos à presença deles...

- E há uma advertência do *Juiz Supremo*, aqui nesse texto, *contra os juízes(as)* que /não julgam o povo com justiça, favorecendo os bandidos, criminosos, ladrões, exploradores, traficantes de drogas, traficantes de pessoas, agiotas oficiais (banqueiros) e não oficiais, abusadores, políticos corruptos de todas as espécies (presidentes, deputados, senadores, prefeitos, governadores, vereadores, representantes de bairros e comunidades,/ etc.

- /E condenam os oprimidos, explorados, pobres, viúvas, órfãos, aflitos, desamparados, sem tetos, (etc.) da nossa sociedade, sobretudo os mais vulneráveis e fracos (*as mulheres e as crianças*...)/

- Quando se levanta um *juiz ou juíza* que julga com retidão, _não por causa do dinheiro ou do suborno a que estão expostos, mas por causa da justiça_ e, sobretudo *do temor* ao *Juiz dos juízes* (o que é raro. Pois, na maioria das vezes, numa sociedade secular, não acreditam em *Deus*)

- O *Senhor* lhes dá o privilégio de serem chamados "*filhos do Altíssimo*" (v. 6)

- No entanto, a verdade "nua e crua" é que sendo *os juízes(as)* _deuses ou demônios_; /crendo ou não crendo na existência/ do *Juiz Supremo* e na *Sua* _Justiça eterna e que não falha_, um dia terão que se apresentarem na *Corte Real do Nosso Senhor Jesus Cristo* para também prestar conta do seu trabalho e serem julgados (v. 1)...

- =Ninguém vai escapar desse dia!= /nenhum juiz humano/ (mesmo que recorram ao recurso da "*Imunidade Política*" diante do *Justo Juiz*) e /nenhuma pessoa viva ou morta/[fn:12]. Os mortos também ressuscitarão para prestar contas...

- O *Senhor* ainda adverte aos juízes, /fazendo-os lembrar que apesar da posição elevada, de honra e respeito que ocupam na sociedade/, _eles não passam de meros mortais e um dia também irão perecer_...

- /Mesmo que pensem que são ainda melhores e mais importantes que os príncipes (ricos, famosos, "influencers" e poderosos da sociedade)/ a quem servem, *morrerão como qualquer um deles* e _terão de comparecer diante_ do *Juiz Supremo*

- *Asafe* nos lembra, no /final do Salmo/, que independentemente da forma de julgar que os juízes(as) humanos escolhem, no final, o *Senhor* é *Aquele* _que se levanta para julgar com justiça, misericórdia e retidão os moradores da terra, pois a *Ele* pertencem todas as Nações (povos) da terra_

** Conclusão

- Com certeza, o foco principal desse *Salmo de Asafe* são *os juízes e juízas da nação de Israel*, e, por extensão, *de todas as nações em todas as épocas*.

- *Vivemos num tempo de grande injustiça e falta de misericórdia*, /inclusive dentro das Instituições Religiosas: cristãs, muçulmanas, budistas, judaicas,/ etc. Ela, a /Injustiça/, passeia diante nossos olhos todo santo dia... Basta ouvir ou ler um noticiário, acessar a notícias na internet, passar a vista nos jornais nas bancas, etc. E você a perceberá quase onipresente...

- Vemos e, às vezes, presenciamos *as mais terríveis situações* cometidas no meio da nossa *sociedade*: /no trabalho, na escola, na faculdade, no meio das nossas famílias, no nosso Estado, país e no mundo... Sobretudo, contra os mais fracos, oprimidos, pobres e desamparados/.

- A *justiça humana* é _subornada e é adquirida a preço de ouro todos os dias e em todos os lugares_. Os /mais abastados facilmente compram-na e a manipulam em favor e lucro próprio/...

- No entanto, o Salmo *adverte os juízes e juízas do nosso tempo* a serem /coerentes com a verdade/: =condenar o erro e aqueles que o cometem= e =absolver as vítimas e inocentes das mãos dos opressores=...

- Enfim, têm o dever de julgar tendo como parâmetro chave a *Palavra de Deus*.  (devemos nos lembrar que todas as Constituições dos países ditos cristãos têm os Mandamentos da Palavra como fundamento).

- A *mensagem desse Salmo*, /além de ser dirigida a pessoas de grande autoridade e responsabilidade diante das sociedades/, _também é direcionada a cada um de nós que somos salvos_ em *Cristo Jesus*.

- Pois, assim como diz o texto, somos também *filhos de Deus*[fn:13] e fomos, /como aqueles/, investidos de *grande poder, grande autoridade e grande responsabilidade* no =Reino= de *Cristo*[fn:14]

- Por isso mesmo, na nossa *lida diária com as pessoas*, de forma geral:

- Com os mais /fracos, as mulheres, as crianças, os necessitados, aqueles com necessidades físicas e mentais especiais, o órfão, a viúva, o explorado, o subjugado, o escravizado, o aprisionado  e acusado injustamente/... e;

- Com nossos *irmãos e irmãs* em *Cristo*...

- Quando julgarmos, *devemos fazê-lo com discernimento, amor e misericórdia*. Pois, do contrário, seremos julgados pelo *Senhor* com o mesmo rigor que julgarmos nosso próximo.

- E não se esqueçam também do lidar com os nossos *inimigos*![fn:15]

- Pois essa é uma das =marcas que diferencia o verdadeiro cristão do religioso=, como disse *Mestre*[fn:16]

- /Que o senhor abençoe a todos nós/


- Cypher
- Sat Jan 14 02:49:26 PM -03 2023
- Para Erratas e correções, por favor, me contactem...

* Footnotes

[fn:1]

- Psalms 82

- Psalms 82:1: Deus está na assembleia divina; julga no meio dos deuses:

- Psalms 82:2: Até quando julgareis injustamente, e tereis respeito às pessoas dos ímpios?

- Psalms 82:3: Fazei justiça ao pobre e ao órfão; procedei retamente com o aflito e o desamparado.

- Psalms 82:4: Livrai o pobre e o necessitado, livrai-os das mãos dos ímpios.

- Psalms 82:5: Eles nada sabem, nem entendem; andam vagueando às escuras; abalam-se todos os fundamentos da terra.

- Psalms 82:6: Eu disse: Vós sois deuses, e filhos do Altíssimo, todos vós.

- Psalms 82:7: Todavia, como homens, haveis de morrer e, como qualquer dos príncipes, haveis de cair.

- Psalms 82:8: Levanta-te, ó Deus, julga a terra; pois a ti pertencem todas as nações. (PorAR)

- See also:
- [[file:2021-10-17-1713 biblical_judgement.org][ biblical_judgement]]

[fn:2]

- Luke 6:37:  [For ver. 37, 38, 41, 42, see Matt. 7:1-5; [Rom. 14:13; 1 Cor. 4:5; James 5:9] , [Matt. 6:14; 18:23-35] Judge not, and you will not be judged; condemn not, and you will not be condemned;  [Matt. 6:14; 18:23-35] forgive, and you will be forgiven; (ESV2011)

[fn:3]

- Matthew 7

- Matthew 7:1: Não julgueis, para que não sejais julgados.

- Matthew 7:2: Porque com o juízo com que julgais, sereis julgados; e com a medida com que medis, vos medirão também.

- Matthew 7:3: E por que vês tu o cisco no olho de teu irmão, e não reparas na trave que está no teu próprio olho?

- Matthew 7:4: Ou como dirás a teu irmão: Deixa-me tirar o cisco do teu olho; e eis que tens uma trave no teu próprio olho?

- Matthew 7:5: Hipócrita! Tira primeiro a trave do teu olho; e então verás claramente para tirar o cisco do olho de teu irmão.

- Matthew 7:6: Não deis aos cães o que é santo, nem lanceis aos porcos as vossas pérolas, para não acontecer que as pisem com os pés e, voltando-se, vos despedacem.

- Matthew 7:7: Pedi, e dar-se-vos-á; buscai, e achareis; batei e abrir-se-vos-á.

- Matthew 7:8: Pois todo o que pede, recebe; e quem busca, acha; e ao que bate, abrir-se-lhe-á.

- Matthew 7:9: Ou qual dentre vós é o homem que, se seu filho lhe pedir pão, lhe dará uma pedra?

- Matthew 7:10: Ou, se lhe pedir peixe, lhe dará uma cobra?

- Matthew 7:11: Se vós, pois, sendo maus, sabeis dar boas dádivas a vossos filhos, quanto mais vosso Pai, que está nos céus, dará boas coisas aos que lhe pedirem?

- Matthew 7:12: Portanto, tudo o que quereis que os homens vos façam, assim fazei-o vós também a eles; porque esta é a lei e os profetas.

- Matthew 7:13: Entrai pela porta estreita; porque larga é a porta, e espaçoso o caminho que conduz para a perdição, e muitos são os que entram por ela;

- Matthew 7:14: e porque estreita é a porta, e apertado o caminho que conduz à vida, e poucos são os que a encontram.

- Matthew 7:15: Guardai-vos dos falsos profetas, que se achegam a vós disfarçados como ovelhas, mas interiormente são lobos devoradores.

- Matthew 7:16: Pelos seus frutos os conhecereis. Colhem-se, porventura, uvas dos espinheiros, ou figos dos abrolhos?

- Matthew 7:17: Assim, toda boa árvore produz bons frutos; porém uma árvore corrupta produz frutos maus.

- Matthew 7:18: Uma árvore boa não pode dar maus frutos; nem uma árvore corrupta dar frutos bons.

- Matthew 7:19: Toda árvore que não produz bom fruto é cortada e lançada no fogo.

- Matthew 7:20: Portanto, pelos seus frutos os conhecereis.

- Matthew 7:21: Nem todo o que me diz: Senhor, Senhor! Entrará no reino dos céus, mas aquele que faz a vontade de meu Pai, que está nos céus.

- Matthew 7:22: Muitos me dirão naquele dia: Senhor, Senhor, não profetizamos nós em teu nome? E em teu nome expelimos demônios? E em teu nome fizemos muitos milagres?

- Matthew 7:23: Então lhes direi claramente: Nunca vos conheci; apartai-vos de mim, vós que praticais a iniquidade.

- Matthew 7:24: Assim, todo aquele que ouve estas minhas palavras e as põe em prática, eu o compararei a um homem sábio, que edificou sua casa sobre a rocha.

- Matthew 7:25: E desceu a chuva, vieram as inundações, sopraram os ventos, e bateram contra aquela casa, e ela não caiu, porque estava fundada sobre a rocha.

- Matthew 7:26: E todo aquele que ouve estas minhas palavras, e não as põe em prática, será comparado a um homem tolo, que edificou sua casa sobre a areia.

- Matthew 7:27: E desceu a chuva, vieram as inundações, sopraram os ventos, e bateram contra aquela casa, e ela caiu, e grande foi a sua queda.

- Matthew 7:28: Ao concluir Jesus este discurso, as multidões se maravilhavam da sua doutrina;

- Matthew 7:29: porque ele as ensinava como quem tem autoridade e não como os escribas. (PorAR)

[fn:4]

- Matthew 13:45-46

- Matthew 13:45: Da mesma maneira, o reino dos céus é semelhante a um negociante que busca boas pérolas;

- Matthew 13:46: e encontrando uma pérola de grande valor, foi, vendeu tudo quanto tinha, e a comprou. (PorAR)

[fn:5]
[[https://pt.wikipedia.org/wiki/Suspensão_do_juízo][Suspensão do Juízo]]

[fn:6]

- Matthew 10:16:  [Luke 10:3; [John 17:18]] Behold, I am sending you out as sheep in the midst of wolves, so be  [Gen. 3:1] wise as serpents and  [Rom. 16:19 (Gk.); Phil. 2:15; [1 Cor. 14:20]] innocent as doves. (ESV2011)

- Matthew 10:16: ιδου εγω αποστελλω υμας ως προβατα εν μεσω λυκων γινεσθε ουν φρονιμοι ως οι οφεις και ακεραιοι ως αι περιστεραι (TR)

- Ἀκέραιος # Strong's number: 00185
- Pronunciation: ak-er'-ah-yos
- KJV defintion: harmless, simpler mansos (lembrando que mansidão não é covardia. Moisés era manso, mas não covarde)

- Φρόνιμος # Strong's number: 05429
- Pronunciation: fron'-ee-mos
- KJV defintion: wise(-r); sábio(s)

[fn:7]

- Documentário da Aljazeera em 3 partes (essa é a terceira parte)

- All Hail The Algorithm : Click Me: The Algorithm Made Me Do It Political manipulation, fake news and lynchings - what’s the cost of our clicks?

- Todos Saldemos o Algoritmo: Clique em Mim: O Algorítimo levou-me a fazer isso - Manipulação Política, notícias falsas e linchamentos - qual é o custo dos nossos cliques?

[fn:8]

- Romans 12:1-2

- Romans 12:1:  [1 Cor. 1:10; 2 Cor. 10:1; Eph. 4:1] I appeal to you therefore, brothers, by the mercies of God,  [ch. 6:13, 16, 19; [Ps. 50:13, 14; 1 Cor. 6:20]; See 1 Pet. 2:5] to present your bodies  [Heb. 10:20] as a living sacrifice, holy and acceptable to God, which is your spiritual worship.

- Romans 12:2:  [1 Pet. 1:14; [1 John 2:15]] Do not be conformed to this world, but be transformed by  [Titus 3:5; [Ps. 51:10; 2 Cor. 4:16; Eph. 4:23; Col. 3:10]] the renewal of your mind, that by testing you may  [Eph. 5:10; 1 Thess. 4:3] discern what is the will of God, what is good and acceptable and perfect. (ESV2011)

[fn:9]

- Matthew 24:23-34

- Matthew 24:23: Se, pois, alguém vos disser: Eis aqui o Cristo! Ou: Ei-lo aí! Não acrediteis;

- Matthew 24:24: porque hão de surgir falsos cristos e falsos profetas, e farão grandes sinais e maravilhas; de modo que, se possível fora, enganariam até aos próprios escolhidos. (PorAR)

[fn:10]

- Tiago 2:12-13

- James 2:12: So speak and so act as those who are to be judged under  [See ch. 1:25] the law of liberty.

- James 2:13: For  [Job 22:6-11; Ps. 18:25, 26; Prov. 21:13; Ezek. 25:11-14; Matt. 6:15; 18:32-35; Luke 6:38] judgment is without mercy to one who has shown no mercy. Mercy triumphs over judgment. (ESV2011)

[fn:11]

- Isaiah 42:3:  [ch. 57:15] a bruised reed he will not break, and a faintly burning wick he will not quench; [Ps. 9:8] he will faithfully bring forth justice. (ESV2011)

- Matthew 12:20: a bruised reed he will not break, and a smoldering wick he will not quench, until he brings justice to victory; (ESV2011)

[fn:12]

-  II Corinthians 5:10: For  [Matt. 25:31, 32; [Rom. 14:10]; See Acts 10:42] we must all appear before the judgment seat of Christ, [See Ps. 62:12] so that each one may receive what is due for what he has done in the body, whether good or evil. (ESV2011)

[fn:13]

- John 1:12: But to all who did receive him,  [See 1 John 5:13] who believed in his name,  [1 John 5:1] he gave the right  [1 John 3:1; [Matt. 5:45]] to become  [[Gal. 3:26]; See ch. 11:52] children of God, (ESV2011)

[fn:14]

- Revelation of John 1:6: and made us  [ch. 5:10; 20:6; 1 Pet. 2:9] a kingdom,  [ch. 5:10; 20:6; 1 Pet. 2:9] priests to  [See Rom. 15:6] his God and Father, to him be  [See Rom. 11:36] glory and  [1 Pet. 4:11] dominion forever and ever. Amen. (ESV2011)

- Revelation of John 1:6: And hath made us kings and priests unto God and his Father; to him be glory and dominion for ever and ever. Amen. (KJVA)

[fn:15]

- Matthew 5:38-47:

- Matthew 5:38 You have heard that it was said, An eye for an eye and a tooth for a tooth.

- Matthew 5:39: But I say to you, Do not resist the one who is evil. But if anyone slaps you on the right cheek, turn to him the other also.

- Matthew 5:40: And if anyone would sue you and take your tunic, let him have your cloak as well.

- Matthew 5:41: And if anyone forces you to go one mile, go with him two miles.

- Matthew 5:42: Give to the one who begs from you, and do not refuse the one who would borrow from you.

- Matthew 5:43: You have heard that it was said, You shall love your neighbor and hate your enemy.

- Matthew 5:44: But I say to you, Love your enemies and pray for those who persecute you,

- Matthew 5:45: so that you may be sons of your Father who is in heaven. For he makes his sun rise on the evil and on the good, and sends rain on the just and on the unjust.

- Matthew 5:46: For if you love those who love you, what reward do you have? Do not even the tax collectors do the same?

- Matthew 5:47: And if you greet only your brothers, what more are you doing than others? Do not even the Gentiles do the same? (ESV2011)

- Romans 12:20: To the contrary,  [Cited from Prov. 25:21, 22; [Ex. 23:4, 5; 2 Kgs. 6:22; Luke 6:27]] if your enemy is hungry, feed him; if he is thirsty, give him something to drink; for by so doing you will heap burning coals on his head. (ESV2011)

- Proverbs 24:17:  Do not rejoice when your enemy falls, and let not your heart be glad when he stumbles,

- Proverbs 24:18: lest the Lord see it and be displeased,
and turn away his anger from him. (ESV2011)

[fn:16]

- Luke 6:32-36

- Luke 6:32:  [Matt. 5:46] If you love those who love you, what benefit is that to you? For even sinners love those who love them.

- Luke 6:33: And if you do good to those who do good to you, what benefit is that to you? For even sinners do the same.

- Luke 6:34: And  [ch. 14:12-14; Prov. 19:17; Matt. 5:42] if you  [Ps. 37:26] lend to those from whom you expect to receive, what credit is that to you? Even sinners lend to sinners, to get back the same amount.

- Luke 6:35: But  [ver. 27] love your enemies, and do good, and lend, expecting nothing in return, and your reward will be great, and  [Matt. 5:45] you will be sons of  [ch. 1:32; See Mark 5:7] the Most High, for  [James 1:5]he is kind to the ungrateful and the evil.

- Luke 6:36:  [Matt. 5:7, 48; Eph. 5:1, 2; James 3:17]Be merciful, even as  [James 5:11] your Father is merciful. (ESV2011)
