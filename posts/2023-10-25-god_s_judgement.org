#+title: God_s_judgement
#+date: <2023-10-25 20:33>
#+description: This is a message about God's judgement based on Psalm 9. I will analyse this Psalm in the light of God's judgement against men, sometimes using natural cataclysms and such

#+filetags: Psalm_9 God judgement bad_news violence corruption exploitation naturedestruction expository preaching Bible study Hermeneutics forensicsliterature Pompeii Vesuvius volcano 79_AD disturbances hurricanes wildfires 

* God's judgement

 - [[https://archive.org/details/gods_judgement][Video is here]]
